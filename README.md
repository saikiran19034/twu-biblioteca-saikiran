#BibliotecaApp - A Library Management System

##How to Run
1. Run `AppCaller` to start the application.
2. User will be prompted for credentials.
3. On successfull authentication user can interact with library.
4. After a user exits from library another user can login.

