package com.twu.biblioteca;

import com.twu.biblioteca.authenticator.Authenticator;
import com.twu.biblioteca.exceptions.InvalidDataException;
import com.twu.biblioteca.io.*;
import com.twu.biblioteca.items.ItemList;
import com.twu.biblioteca.sections.BookSection;
import com.twu.biblioteca.sections.LibrarySection;
import com.twu.biblioteca.sections.MovieSection;
import com.twu.biblioteca.user.UserList;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class AppCaller {
    public static void main(String[] args) throws FileNotFoundException, InvalidDataException {
        String booksFile = System.getProperty("user.dir") + "/resources" + "/books";
        String moviesFile = System.getProperty("user.dir") + "/resources" + "/movies";
        String userdata = System.getProperty("user.dir") + "/resources" + "/users";

        File fileWithListOfBooks = new File(booksFile);
        Scanner fileScanner = new Scanner(fileWithListOfBooks);
        FileInput fileInput = new FileInput(fileScanner);
        Loader bookLoader = new Loader(fileInput, new BookReader());
        ItemList listOfBooks = bookLoader.load();

        File fileWithMovies = new File(moviesFile);
        Scanner movieScanner = new Scanner(fileWithMovies);
        fileInput = new FileInput(movieScanner);
        Loader movieLoader = new Loader(fileInput, new MovieReader());
        ItemList listOfMovies = movieLoader.load();

        LibrarySection books = new BookSection(listOfBooks);
        LibrarySection movies = new MovieSection(listOfMovies);

        Biblioteca library = new Biblioteca(books, movies);
        OutputTerminal stdout = new StandardOutput();
        InputTerminal stdin = new StandardInput();

        File userData = new File(userdata);
        Scanner userScanner = new Scanner(userData);
        UserList userList = new UserDataLoader(new FileInput(userScanner)).load();
        Authenticator session = new Authenticator(userList);

        BibliotecaApp bibliotecaApp = new BibliotecaApp(session, stdin, stdout, library);
        stdout.write(bibliotecaApp.welcomeMessage());
        bibliotecaApp.start();
    }
}
