package com.twu.biblioteca;

import com.twu.biblioteca.sections.LibrarySection;
import com.twu.biblioteca.user.User;

public class Biblioteca {
    private LibrarySection books;
    private LibrarySection movies;

    Biblioteca(LibrarySection books, LibrarySection movies) {
        this.books = books;
        this.movies = movies;
    }

    public String availableBooks() {
        return books.availableItems();
    }

    public boolean checkOutBook(String name, User loggedInUser) {
        return books.checkOut(name, loggedInUser);
    }

    public boolean checkInBook(String name, User loggedInUser) {
        return books.checkIn(name, loggedInUser);
    }

    public String checkedOutBooks(User loggedInUser) {
        return books.checkedOutItems(loggedInUser);

    }

    public String availableMovies() {
        return movies.availableItems();
    }

    public boolean checkOutMovie(String name, User loggedInUser) {
        return movies.checkOut(name, loggedInUser);
    }

}
