package com.twu.biblioteca;

import com.twu.biblioteca.commands.*;
import com.twu.biblioteca.authenticator.Authenticator;
import com.twu.biblioteca.io.InputTerminal;
import com.twu.biblioteca.io.OutputTerminal;
import com.twu.biblioteca.menu.Menu;
import com.twu.biblioteca.menu.Option;
import com.twu.biblioteca.user.User;

//Driver of application
public class BibliotecaApp {
    private Authenticator sessionAuthenticator;
    private InputTerminal in;
    private OutputTerminal out;
    private Biblioteca library;

    BibliotecaApp(Authenticator sessionAuthenticator, InputTerminal in, OutputTerminal
            out, Biblioteca library) {
        this.sessionAuthenticator = sessionAuthenticator;
        this.in = in;
        this.out = out;
        this.library = library;
    }

    void start() {
        String quit;
        do {
            User loggedInUser = getLoggedInUser();
            if (loggedInUser != null) {
                Menu menu = createMenuForUser(loggedInUser);
                menu.drive();
            } else {
                out.write("Invalid Credentials");
            }
            out.write("Type `quit` to quit from app or any character to " +
                    "continue to login: ");
            quit = in.nextLine();
        }
        while (!quit.equals("quit"));
    }

    private Menu createMenuForUser(User loggedInUser) {
        Menu menu = new Menu(in, out);
        menu.addItem(new Option("0", "Exit"), new Exit(menu));
        menu.addItem(new Option("1", "ListBooks"), new ListBooks(in, out, library));
        menu.addItem(new Option("2", "CheckoutBook"), new CheckOutBook(in, out, library, loggedInUser));
        menu.addItem(new Option("3", "Checkin Book"), new CheckInBook(in, out, library, loggedInUser));
        menu.addItem(new Option("4", "List Movies"), new ListMovies(out, library));
        menu.addItem(new Option("5", "Checkout Movie"), new CheckoutMovie(in, out, library, loggedInUser));
        menu.addItem(new Option("6", "List my details"), new ListUserData(out, loggedInUser));
        return menu;
    }

    private User getLoggedInUser() {
        out.write("UserId :");
        String userId = in.nextLine();
        out.write("Password:");
        String password = in.nextLine();

        return sessionAuthenticator.authenticate(userId, password);
    }

    String welcomeMessage() {
        return "Welcome to Biblioteca !";
    }

}
