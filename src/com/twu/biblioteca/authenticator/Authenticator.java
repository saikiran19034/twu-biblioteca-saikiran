package com.twu.biblioteca.authenticator;

import com.twu.biblioteca.user.User;
import com.twu.biblioteca.user.UserList;

//Know to how to permit user into library
public class Authenticator {
    private UserList users;

    public Authenticator(UserList userList) {
        users = userList;
    }

    public User authenticate(String userId, String password) {
        if (users.validate(userId, password)) {
            return users.userWithId(userId);
        }
        return null;
    }
}
