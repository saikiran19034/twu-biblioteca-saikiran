package com.twu.biblioteca.commands;

import com.twu.biblioteca.*;
import com.twu.biblioteca.io.InputTerminal;
import com.twu.biblioteca.io.OutputTerminal;
import com.twu.biblioteca.user.User;

public class CheckInBook implements Command {
    private Biblioteca library;
    private User loggedInUser;
    private InputTerminal in;
    private OutputTerminal out;

    public CheckInBook(InputTerminal in, OutputTerminal out, Biblioteca library, User loggedInUser) {
        this.in = in;
        this.out = out;
        this.library = library;
        this.loggedInUser = loggedInUser;
    }

    @Override
    public void execute() {
        out.write("Your Checked out Book List");
        out.write(library.checkedOutBooks(loggedInUser));
        out.write("Choose Item: ");
        if (library.checkInBook(in.nextLine(),loggedInUser)) {
            out.write("Thank you for returning the book.");
            return;
        }
        out.write("That is not a valid book to return.");
    }
}
