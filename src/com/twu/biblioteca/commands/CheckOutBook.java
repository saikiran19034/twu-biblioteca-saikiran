package com.twu.biblioteca.commands;

import com.twu.biblioteca.*;
import com.twu.biblioteca.io.InputTerminal;
import com.twu.biblioteca.io.OutputTerminal;
import com.twu.biblioteca.user.User;

public class CheckOutBook implements Command {
    private Biblioteca library;
    private User loggedInUser;
    private InputTerminal in;
    private OutputTerminal out;

    public CheckOutBook(InputTerminal in, OutputTerminal out, Biblioteca library,
                        User loggedInUser) {
        this.in = in;
        this.out = out;
        this.library = library;
        this.loggedInUser = loggedInUser;
    }

    @Override
    public void execute() {
        out.write("Available Books:");
        out.write(library.availableBooks());
        out.write("Choose Book: ");
        if (library.checkOutBook(in.nextLine(), loggedInUser)) {
            out.write("Thank you! Enjoy the book");
            return;
        }
        out.write("That book is not available.");
    }
}
