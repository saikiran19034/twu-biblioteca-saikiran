package com.twu.biblioteca.commands;

import com.twu.biblioteca.*;
import com.twu.biblioteca.io.InputTerminal;
import com.twu.biblioteca.io.OutputTerminal;
import com.twu.biblioteca.user.User;

public class CheckoutMovie implements Command {

    private Biblioteca library;
    private User loggedInUser;
    private InputTerminal in;
    private OutputTerminal out;

    public CheckoutMovie(InputTerminal in, OutputTerminal out, Biblioteca library,
                         User loggedInUser) {
        this.in = in;
        this.out = out;
        this.library = library;
        this.loggedInUser = loggedInUser;
    }

    @Override
    public void execute() {
        out.write("Available Movies:");
        out.write(library.availableMovies());
        out.write("Choose Movie: ");
        if (library.checkOutMovie(in.nextLine(), loggedInUser)) {
            out.write("Thank you! Enjoy the movie");
            return;
        }
        out.write("Invalid Movie Name");
    }
}
