package com.twu.biblioteca.commands;

public interface Command {
    void execute();
}
