package com.twu.biblioteca.commands;

import com.twu.biblioteca.menu.Menu;

public class Exit implements Command {
    Menu receiver;

    public Exit(Menu receiver) {
        this.receiver = receiver;
    }

    @Override
    public void execute() {
        receiver.quitMenu();
    }
}
