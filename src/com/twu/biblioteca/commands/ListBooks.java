package com.twu.biblioteca.commands;

import com.twu.biblioteca.Biblioteca;
import com.twu.biblioteca.io.InputTerminal;
import com.twu.biblioteca.io.OutputTerminal;

public class ListBooks implements Command {
    private Biblioteca library;
    InputTerminal in;
    private OutputTerminal out;

    public ListBooks(InputTerminal in, OutputTerminal out, Biblioteca library) {
        this.in = in;
        this.out = out;
        this.library = library;
    }

    @Override
    public void execute() {
        out.write("Available Books");
        out.write(library.availableBooks());
    }
}
