package com.twu.biblioteca.commands;

import com.twu.biblioteca.Biblioteca;
import com.twu.biblioteca.io.OutputTerminal;

public class ListMovies implements Command {
    private Biblioteca library;
    private OutputTerminal out;

    public ListMovies(OutputTerminal out, Biblioteca library) {
        this.out = out;
        this.library = library;
    }

    @Override
    public void execute() {
        out.write("Available Movies");
        out.write(library.availableMovies());
    }
}
