package com.twu.biblioteca.commands;

import com.twu.biblioteca.io.OutputTerminal;
import com.twu.biblioteca.user.User;

public class ListUserData implements Command {
    private OutputTerminal out;
    User loggedInUser;

    public ListUserData(OutputTerminal out, User loggedInUser) {
        this.out = out;
        this.loggedInUser = loggedInUser;
    }

    @Override
    public void execute() {
        out.write("Details: ");
        out.write(loggedInUser.toString());
    }
}
