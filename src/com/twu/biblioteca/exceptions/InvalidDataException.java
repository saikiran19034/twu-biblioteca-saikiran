package com.twu.biblioteca.exceptions;

public class InvalidDataException extends Throwable {
    private InvalidDataException(String message) {
        super(message);
    }

    public static InvalidDataException invalidBookDataException() {
        return new InvalidDataException("Invalid Book Data!");
    }

    public static InvalidDataException invalidMovieDataException() {
        return new InvalidDataException("Invalid Movie Data!");
    }

    public static InvalidDataException invalidUserDataException() {
        return new InvalidDataException("Invalid User Data!");
    }
}
