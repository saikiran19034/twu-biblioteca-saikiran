package com.twu.biblioteca.io;

import com.twu.biblioteca.items.Book;
import com.twu.biblioteca.exceptions.InvalidDataException;
import com.twu.biblioteca.items.Item;

public class BookReader implements Reader {
    @Override
    public Item readFrom(String line) throws InvalidDataException {
        String[] components = line.split("\\|");
        if (components.length != 3)
            throw InvalidDataException.invalidBookDataException();
        String author = components[0];
        String name = components[1];
        int year = Integer.parseInt(components[2]);
        return new Book(author, name, year);
    }
}
