package com.twu.biblioteca.io;

import java.util.Scanner;

public class FileInput implements InputTerminal {
    private Scanner in;

    public FileInput(Scanner in) {
        this.in = in;
    }

    @Override
    public int nextInteger() {
        return in.nextInt();
    }

    @Override
    public String nextLine() {
        return in.nextLine();
    }

    @Override
    public boolean hasNextLine() {
        return in.hasNextLine();
    }
}
