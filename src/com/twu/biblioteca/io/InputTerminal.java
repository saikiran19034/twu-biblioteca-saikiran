package com.twu.biblioteca.io;

//Understands how to read
public interface InputTerminal {
    int nextInteger();

    String nextLine();

    boolean hasNextLine();
}
