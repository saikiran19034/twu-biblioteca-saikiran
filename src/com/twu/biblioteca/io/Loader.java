package com.twu.biblioteca.io;

import com.twu.biblioteca.exceptions.InvalidDataException;
import com.twu.biblioteca.items.ItemList;

public class Loader {
    InputTerminal in;
    private Reader reader;

    public Loader(InputTerminal in, Reader reader) {
        this.in = in;
        this.reader = reader;
    }

    public ItemList load() throws InvalidDataException {
        ItemList items = new ItemList();
        while (in.hasNextLine()) {
            items.add(reader.readFrom(in.nextLine()));
        }
        return items;
    }
}
