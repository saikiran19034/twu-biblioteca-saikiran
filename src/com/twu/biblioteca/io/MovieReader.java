package com.twu.biblioteca.io;

import com.twu.biblioteca.exceptions.InvalidDataException;
import com.twu.biblioteca.items.Item;
import com.twu.biblioteca.items.Movie;

public class MovieReader implements Reader {
    @Override
    public Item readFrom(String line) throws InvalidDataException {
        String[] components = line.split("\\|");
        if (components.length != 4)
            throw InvalidDataException.invalidMovieDataException();
        String director = components[0];
        String name = components[1];
        int year = Integer.parseInt(components[2]);
        String rating = components[3];
        return new Movie(director, name, year, rating);
    }
}
