package com.twu.biblioteca.io;

//Understands how to deliver output
public interface OutputTerminal {
    void write(String line);
}
