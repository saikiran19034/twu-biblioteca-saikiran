package com.twu.biblioteca.io;

import com.twu.biblioteca.exceptions.InvalidDataException;
import com.twu.biblioteca.items.Item;

public interface Reader {
    Item readFrom(String line) throws InvalidDataException;
}
