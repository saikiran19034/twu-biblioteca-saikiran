package com.twu.biblioteca.io;

import java.util.Scanner;

//Understands how to read from standard input
public class StandardInput implements InputTerminal {
    final Scanner stdin = new Scanner(System.in);

    public int nextInteger() {
        return Integer.parseInt(stdin.nextLine());
    }

    public String nextLine() {
        return stdin.nextLine();
    }

    @Override
    public boolean hasNextLine() {
        return stdin.hasNextLine();
    }

}
