package com.twu.biblioteca.io;

import com.twu.biblioteca.io.OutputTerminal;

//Understands how to deliver output to standard output
public class StandardOutput implements OutputTerminal {
    @Override
    public void write(String line) {
        System.out.println(line);
    }
}
