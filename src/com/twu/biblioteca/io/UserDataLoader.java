package com.twu.biblioteca.io;

import com.twu.biblioteca.exceptions.InvalidDataException;
import com.twu.biblioteca.user.User;
import com.twu.biblioteca.user.UserList;

public class UserDataLoader {
    InputTerminal in;

    public UserDataLoader(InputTerminal in) {
        this.in = in;
    }

    public UserList load() throws InvalidDataException {
        UserList list = new UserList();
        while (in.hasNextLine()) {
            String[] fields = in.nextLine().split("\\|");
            if (fields.length != 5) {
                throw InvalidDataException.invalidUserDataException();
            }
            list.add(new User(fields[0], fields[1], fields[2], fields[3],
                    fields[4]));
        }
        return list;
    }
}
