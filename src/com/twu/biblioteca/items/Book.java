package com.twu.biblioteca.items;

//Knows details about book
public class Book implements Item {
    private String name;
    private String author;
    private int yearPublished;

    public Book(String author, String name, int yearPublished) {
        this.name = name;
        this.author = author;
        this.yearPublished = yearPublished;
    }

    @Override
    public String toString() {
        return String.format("%-15s\t%-40.40s\t%d", author, name, yearPublished);
    }

    @Override
    public boolean hasSameName(String name) {
        return this.name.equalsIgnoreCase(name);
    }
}
