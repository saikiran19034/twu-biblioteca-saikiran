package com.twu.biblioteca.items;

public interface Item {
    boolean hasSameName(String name);
}
