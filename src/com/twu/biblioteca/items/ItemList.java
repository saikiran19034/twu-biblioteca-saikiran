package com.twu.biblioteca.items;

import java.util.ArrayList;

public class ItemList extends ArrayList<Item> {
    public Item itemWithSameName(String name) {
        for (Item item : this) {
            if (item.hasSameName(name))
                return item;
        }
        return null;
    }

    public String toString() {
        StringBuilder list = new StringBuilder();
        for (Item item : this) {
            list.append(item);
            list.append("\n");
        }
        return list.toString();
    }
}
