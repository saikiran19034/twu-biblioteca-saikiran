package com.twu.biblioteca.items;

public class Movie implements Item {
    private String name;
    private String director;
    private String rating;
    private int year;

    public Movie(String director, String name, int year, String rating) {
        this.name = name;
        this.director = director;
        this.rating = rating;
        this.year = year;
    }

    public String toString() {
        return String.format("%-15s\t%-40.40s\t%d\t%s", director, name, year,
                rating);
    }

    @Override
    public boolean hasSameName(String name) {
        return this.name.equalsIgnoreCase(name);
    }
}
