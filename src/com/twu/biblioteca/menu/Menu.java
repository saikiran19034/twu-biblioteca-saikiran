package com.twu.biblioteca.menu;

import com.twu.biblioteca.commands.Command;
import com.twu.biblioteca.io.InputTerminal;
import com.twu.biblioteca.io.OutputTerminal;

import java.util.LinkedHashMap;

public class Menu {
    private LinkedHashMap<Option, Command> list = new LinkedHashMap<Option, Command>();
    private InputTerminal in;
    private OutputTerminal out;
    private boolean notQuit;

    public Menu(InputTerminal in, OutputTerminal out) {
        this.in = in;
        this.out = out;
        notQuit = true;
    }

    public void drive() {
        while (notQuit) {
            display();
            out.write("Choose Option:");
            executeCommand(in.nextLine());
        }
    }

    private void executeCommand(String opt) {
        for (Option op : list.keySet()) {
            if (op.isMatcherSame(opt)) {
                list.get(op).execute();
                return;
            }
        }
        out.write("Invalid Option!");
    }

    public void quitMenu() {
        notQuit = false;
    }


    public void addItem(Option op, Command cmd) {
        list.put(op, cmd);
    }

    private void display() {
        out.write("MENU");
        for (Option op : list.keySet()) {
            out.write(op.toString());
        }
    }
}
