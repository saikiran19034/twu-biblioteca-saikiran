package com.twu.biblioteca.menu;

public class Option {
    private String description;
    private String identifier;

    public Option(String identifier, String description) {
        this.description = description;
        this.identifier = identifier;
    }

    public boolean isMatcherSame(String matcher) {
        return this.identifier.equalsIgnoreCase(matcher);
    }

    @Override
    public String toString() {
        return identifier + " " + description;
    }
}
