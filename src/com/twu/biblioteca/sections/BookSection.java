package com.twu.biblioteca.sections;

import com.twu.biblioteca.items.Item;
import com.twu.biblioteca.items.ItemList;
import com.twu.biblioteca.user.User;

import java.util.HashMap;

public class BookSection implements LibrarySection {
    private ItemList availableBooks;
    private HashMap<Item, User> whoCheckedOut = new HashMap<Item, User>();

    public BookSection(ItemList books) {
        this.availableBooks = books;
    }

    @Override
    public boolean checkOut(String name, User loggedInUser) {
        Item book = availableBooks.itemWithSameName(name);
        if (book == null)
            return false;
        whoCheckedOut.put(book, loggedInUser);
        availableBooks.remove(book);
        return true;
    }

    private Item checkedOutBookWithName(String name) {
        for (Item checkedOutBook : whoCheckedOut.keySet()) {
            if (checkedOutBook.hasSameName(name)) {
                return checkedOutBook;
            }
        }
        return null;
    }

    @Override
    public boolean checkIn(String name, User loggedInUser) {
        Item book = checkedOutBookWithName(name);
        if (book == null) {
            return false;
        }
        if (!whoCheckedOut.get(book).equals(loggedInUser)) {
            return false;
        }
        availableBooks.add(book);
        whoCheckedOut.remove(book);
        return true;
    }

    @Override
    public String availableItems() {
        return availableBooks.toString();
    }

    @Override
    public String checkedOutItems(User loggedInUser) {
        StringBuffer list = new StringBuffer();
        for (Item checkedOutBook : whoCheckedOut.keySet()) {
            if (whoCheckedOut.get(checkedOutBook).equals(loggedInUser)) {
                list.append(checkedOutBook.toString()).append("\n");
            }
        }
        return list.toString();
    }

    public String whoCheckedOutList() {
        StringBuffer list = new StringBuffer();
        for (Item item : whoCheckedOut.keySet()) {
            list.append(item).append("==>").append(whoCheckedOut.get(item))
                    .append("\n");
        }
        return list.toString();
    }
}
