package com.twu.biblioteca.sections;

import com.twu.biblioteca.user.User;

public interface LibrarySection {
    boolean checkOut(String name,User loggedInUser);
    boolean checkIn(String name,User loggedInUser);
    String availableItems();
    String checkedOutItems(User loggedInUser);
}
