package com.twu.biblioteca.sections;

import com.twu.biblioteca.items.Item;
import com.twu.biblioteca.items.ItemList;
import com.twu.biblioteca.user.User;

import java.util.HashMap;

public class MovieSection implements LibrarySection {
    private ItemList availableMovies;
    private HashMap<Item, User> whoCheckedOut = new HashMap<Item, User>();

    public MovieSection(ItemList movies) {
        availableMovies = movies;
    }

    @Override
    public boolean checkOut(String name, User loggedInUser) {
        Item book = availableMovies.itemWithSameName(name);
        if (book == null)
            return false;
        whoCheckedOut.put(book, loggedInUser);
        availableMovies.remove(book);
        return true;
    }

    private Item checkedOutMovieWithName(String name) {
        for (Item checkedOutMovie : whoCheckedOut.keySet()) {
            if (checkedOutMovie.hasSameName(name)) {
                return checkedOutMovie;
            }
        }
        return null;
    }

    @Override
    public boolean checkIn(String name, User loggedInUser) {
        Item movie = checkedOutMovieWithName(name);
        if (movie == null) {
            return false;
        }
        if (!whoCheckedOut.get(movie).equals(loggedInUser)) {
            return false;
        }
        availableMovies.add(movie);
        whoCheckedOut.remove(movie);
        return true;
    }

    @Override
    public String availableItems() {
        return availableMovies.toString();
    }


    @Override
    public String checkedOutItems(User loggedInUser) {
        StringBuffer list = new StringBuffer();
        for (Item checkedOutMovie : whoCheckedOut.keySet()) {
            if (whoCheckedOut.get(checkedOutMovie).equals(loggedInUser)) {
                list.append(checkedOutMovie.toString()).append("\n");
            }
        }
        return list.toString();
    }

    public String whoCheckedOutList() {
        StringBuffer list = new StringBuffer();
        for (Item item : whoCheckedOut.keySet()) {
            list.append(item).append("==>").append(whoCheckedOut.get(item))
                    .append("\n");
        }
        return list.toString();
    }
}
