package com.twu.biblioteca.user;

public class User {
    private String userId;
    private String password;
    private String email;
    private String name;
    private String phoneNo;

    public static User dummy = new User("", "", "", "", "");

    public User(String userId, String password, String name, String email, String phoneNo) {
        this.userId = userId;
        this.password = password;
        this.name = name;
        this.email = email;
        this.phoneNo = phoneNo;
    }

    public boolean isSameUser(String userId, String password) {
        return this.userId.equalsIgnoreCase(userId) && this.password.equals
                (password);
    }

    public boolean isSameUser(User other) {
        return this.userId.equalsIgnoreCase(other.userId);
    }

    public boolean hasSameUserId(String userId) {
        return this.userId.equalsIgnoreCase(userId);
    }

    @Override
    public String toString() {
        return "UserId: " + userId + "\tName: " + name + "\tEmail: " +
                email + "\tPhone: " + phoneNo;
    }
}
