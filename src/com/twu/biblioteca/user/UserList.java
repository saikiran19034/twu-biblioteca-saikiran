package com.twu.biblioteca.user;

import java.util.ArrayList;

public class UserList extends ArrayList<User> {
    public boolean validate(String name, String password) {
        for (User user : this) {
            if (user.isSameUser(name, password)) {
                return true;
            }
        }
        return false;
    }

    public User userWithId(String userId) {
        for (User user : this) {
            if (user.hasSameUserId(userId))
                return user;
        }
        return User.dummy;
    }

    @Override
    public String toString() {
        StringBuffer list = new StringBuffer();
        for (User user : this) {
            list.append(user).append("\n");
        }
        return list.toString();
    }

    @Override
    public boolean contains(Object o) {
        User test = (User) o;
        for (User user : this) {
            if (user.toString().equals(test.toString())) {
                return true;
            }
        }
        return false;
    }
}
