package com.twu.biblioteca;

import com.twu.biblioteca.authenticator.Authenticator;
import com.twu.biblioteca.user.User;
import com.twu.biblioteca.user.UserList;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class AuthenticatorTest {
    @Test
    public void shouldReturnTrueIfUserGaveValidCredentials() {
        User user = mock(User.class);
        UserList list = mock(UserList.class);
        Authenticator auth = new Authenticator(list);
        when(list.validate("000-1111", "pwd")).thenReturn(true);
        when(list.userWithId("000-1111")).thenReturn(user);
        assertEquals(user, auth.authenticate("000-1111", "pwd"));
    }

    @Test
    public void shouldReturnFalseIfUserEnterInvalidCredentials() {
        User user = mock(User.class);
        UserList list = mock(UserList.class);
        Authenticator auth = new Authenticator(list);
        when(list.validate("000-1111", "pwd")).thenReturn(false);
        when(list.userWithId("000-1111")).thenReturn(user);
        assertNull(auth.authenticate("000-1111", "pwd"));
    }
}
