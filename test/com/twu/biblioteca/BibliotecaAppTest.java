package com.twu.biblioteca;

import com.twu.biblioteca.authenticator.Authenticator;
import com.twu.biblioteca.io.InputTerminal;
import com.twu.biblioteca.io.OutputTerminal;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testng.AssertJUnit.assertEquals;

public class BibliotecaAppTest {
    @Test
    public void shouldDisplayWelcomeMessageToUser() {
        Authenticator sessionAuthenticator = mock(Authenticator.class);
        InputTerminal stdin = mock(InputTerminal.class);
        OutputTerminal stdout = mock(OutputTerminal.class);
        Biblioteca library = mock(Biblioteca.class);
        BibliotecaApp bibliotecaApp = new BibliotecaApp(sessionAuthenticator, stdin, stdout, library);
        assertEquals("Welcome to Biblioteca !", bibliotecaApp.welcomeMessage());
    }

    @Test
    public void shouldCallAuthenticatorForAuthenticatingUser() {
        Authenticator sessionAuthenticator = mock(Authenticator.class);
        InputTerminal stdin = mock(InputTerminal.class);
        OutputTerminal stdout = mock(OutputTerminal.class);
        Biblioteca library = mock(Biblioteca.class);
        BibliotecaApp bibliotecaApp = new BibliotecaApp(sessionAuthenticator, stdin, stdout, library);
        when(stdin.nextLine()).thenReturn("000-1111").thenReturn("pwd")
                .thenReturn("quit");
        bibliotecaApp.start();
        verify(sessionAuthenticator).authenticate("000-1111", "pwd");
    }

    @Test
    public void
    shouldDisplayInvalidCredentialsWhenUserEnteredInvalidCredentials() {
        Authenticator sessionAuthenticator = mock(Authenticator.class);
        InputTerminal stdin = mock(InputTerminal.class);
        OutputTerminal stdout = mock(OutputTerminal.class);
        Biblioteca library = mock(Biblioteca.class);
        BibliotecaApp bibliotecaApp = new BibliotecaApp(sessionAuthenticator, stdin, stdout, library);
        when(stdin.nextLine()).thenReturn("000-1111").thenReturn("pwd")
                .thenReturn("quit");
        when(sessionAuthenticator.authenticate("000-1111", "pwd")).thenReturn
                (null);
        bibliotecaApp.start();
        verify(stdout).write("Invalid Credentials");
    }
}
