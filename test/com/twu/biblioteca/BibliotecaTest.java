package com.twu.biblioteca;

import com.twu.biblioteca.sections.BookSection;
import com.twu.biblioteca.sections.LibrarySection;
import com.twu.biblioteca.sections.MovieSection;
import com.twu.biblioteca.user.User;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class BibliotecaTest {

    private Biblioteca myLibrary;
    private LibrarySection bookSection;
    private LibrarySection movieSection;
    private User user;

    @Before
    public void setUp() {
        user = mock(User.class);
        bookSection = mock(BookSection.class);
        movieSection = mock(MovieSection.class);
    }

    @Test
    public void libraryShouldCallMovieSectionForMovieList() {
        Biblioteca myLibrary = new Biblioteca(bookSection, movieSection);
        myLibrary.availableMovies();
        verify(movieSection).availableItems();
    }

    @Test
    public void libraryShouldCallMovieSectionToCheckOutMovie() {
        User user = mock(User.class);
        Biblioteca myLibrary = new Biblioteca(bookSection, movieSection);
        String movie = "movie";
        myLibrary.checkOutMovie(movie, user);
        verify(movieSection).checkOut(movie, user);
    }

    @Test
    public void libraryShouldCallBooksSectionToListBooks() {
        Biblioteca myLibrary = new Biblioteca(bookSection, movieSection);
        myLibrary.availableBooks();
        verify(bookSection).availableItems();
    }

    @Test
    public void libraryShouldCallBookSectionToCheckOutBook() {
        Biblioteca myLibrary = new Biblioteca(bookSection, movieSection);
        String book = "book";
        myLibrary.checkOutBook(book, user);
        verify(bookSection).checkOut(book, user);
    }

    @Test
    public void libraryShouldCallBookSectionToCheckInBook() {
        Biblioteca myLibrary = new Biblioteca(bookSection, movieSection);
        String name = "book";
        myLibrary.checkInBook(name, user);
        verify(bookSection).checkIn(name, user);
    }

    @Test
    public void shouldCallBookSectionForCheckedOutList() {
        Biblioteca myLibrary = new Biblioteca(bookSection, movieSection);
        myLibrary.checkedOutBooks(user);
        verify(bookSection).checkedOutItems(user);
    }
}
