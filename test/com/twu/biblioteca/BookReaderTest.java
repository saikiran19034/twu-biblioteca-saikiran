package com.twu.biblioteca;

import com.twu.biblioteca.exceptions.InvalidDataException;
import com.twu.biblioteca.io.BookReader;
import com.twu.biblioteca.items.Item;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class BookReaderTest {
    @Test
    public void shouldNotThrowExceptionIfFormatIsCorrect() throws InvalidDataException {
        BookReader reader = new BookReader();
        reader.readFrom("Sai|Hello|2013");
    }

    @Test(expected = InvalidDataException.class)
    public void shouldThrowExceptionIfFormatIsInvalid() throws InvalidDataException {
        BookReader reader = new BookReader();
        reader.readFrom("Sai|Hello2013");
    }

    @Test
    public void shouldReturnBookObjectAfterParsing() throws
            InvalidDataException {
        BookReader reader = new BookReader();
        Item book = reader.readFrom("Sai|Hello|2013");
        assertTrue(book.hasSameName("Hello"));
    }
}
