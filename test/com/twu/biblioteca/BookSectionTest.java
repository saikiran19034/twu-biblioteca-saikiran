package com.twu.biblioteca;

import com.twu.biblioteca.items.Item;
import com.twu.biblioteca.items.ItemList;
import com.twu.biblioteca.items.Movie;
import com.twu.biblioteca.sections.BookSection;
import com.twu.biblioteca.user.User;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BookSectionTest {
    @Test
    public void shouldCheckOutBookIfNameIsValid() {
        ItemList list = mock(ItemList.class);
        User user = mock(User.class);
        Item harry = mock(Item.class);
        when(list.itemWithSameName("harry")).thenReturn(harry);
        BookSection section = new BookSection(list);
        assertTrue(section.checkOut("harry", user));
    }

    @Test
    public void shouldNotCheckOutBookIfNameIsInValid() {
        ItemList list = mock(ItemList.class);
        User user = mock(User.class);
        Item harry = mock(Item.class);
        when(list.itemWithSameName("harry")).thenReturn(harry);
        BookSection section = new BookSection(list);
        assertFalse(section.checkOut("hey", user));
    }

    @Test
    public void userShouldBeAbleToCheckInTheBookHeCheckedOut() {
        ItemList list = mock(ItemList.class);
        User user = mock(User.class);
        Item harry = mock(Item.class);
        when(harry.hasSameName("harry")).thenReturn(true);
        when(list.itemWithSameName("harry")).thenReturn(harry);
        when(user.isSameUser(user)).thenReturn(true);
        BookSection section = new BookSection(list);
        section.checkOut("harry", user);
        assertTrue(section.checkIn("harry", user));
    }
    @Test
    public void shouldReturnStringContainingBookAndBorrower() {
        ItemList list = mock(ItemList.class);
        User user = mock(User.class);
        when(user.toString()).thenReturn("Hero");
        Item harry = new Movie("some one", "harry", 2003, "9");
        when(list.itemWithSameName("harry")).thenReturn(harry);
        BookSection section = new BookSection(list);
        section.checkOut("harry", user);
        assertEquals(section.whoCheckedOutList(), harry.toString() + "==>" + "Hero\n");
    }
}
