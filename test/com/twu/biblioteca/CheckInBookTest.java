package com.twu.biblioteca;

import com.twu.biblioteca.commands.CheckInBook;
import com.twu.biblioteca.io.InputTerminal;
import com.twu.biblioteca.io.OutputTerminal;
import com.twu.biblioteca.user.User;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CheckInBookTest {
    @Test
    public void commandShouldCallLibraryToCheckInBook() {
        InputTerminal in = mock(InputTerminal.class);
        OutputTerminal out = mock(OutputTerminal.class);
        Biblioteca library = mock(Biblioteca.class);
        User loggedInUser = mock(User.class);
        CheckInBook command = new CheckInBook(in, out, library, loggedInUser);
        when(in.nextLine()).thenReturn("book");
        command.execute();
        verify(library).checkInBook("book",loggedInUser);
    }

    @Test
    public void shouldDisplayCheckedOutBooksByThatUserWhileCheckingIn(){
        InputTerminal in = mock(InputTerminal.class);
        OutputTerminal out = mock(OutputTerminal.class);
        Biblioteca library = mock(Biblioteca.class);
        User loggedInUser = mock(User.class);
        CheckInBook command = new CheckInBook(in, out, library, loggedInUser);
        command.execute();
        verify(library).checkedOutBooks(loggedInUser);
    }
}
