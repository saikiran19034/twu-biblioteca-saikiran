package com.twu.biblioteca;

import com.twu.biblioteca.commands.CheckOutBook;
import com.twu.biblioteca.io.InputTerminal;
import com.twu.biblioteca.io.OutputTerminal;
import com.twu.biblioteca.user.User;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CheckOutBookTest {
    @Test
    public void commandShouldCallLibraryToCheckOutBook() {
        InputTerminal in = mock(InputTerminal.class);
        OutputTerminal out = mock(OutputTerminal.class);
        Biblioteca library = mock(Biblioteca.class);
        User loggedInUser = mock(User.class);
        CheckOutBook command = new CheckOutBook(in, out, library, loggedInUser);
        when(in.nextLine()).thenReturn("book");
        command.execute();
        verify(library).checkOutBook("book",loggedInUser);
    }
    @Test
    public void shouldDisplayCheckedOutBooksByThatUserWhileCheckingOut(){
        InputTerminal in = mock(InputTerminal.class);
        OutputTerminal out = mock(OutputTerminal.class);
        Biblioteca library = mock(Biblioteca.class);
        User loggedInUser = mock(User.class);
        CheckOutBook command = new CheckOutBook(in, out, library, loggedInUser);
        command.execute();
        verify(library).availableBooks();
    }
}
