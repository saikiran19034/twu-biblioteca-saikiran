package com.twu.biblioteca;

import com.twu.biblioteca.items.Item;
import com.twu.biblioteca.items.ItemList;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ItemListTest {
    @Test
    public void ifWeCheckAItemInListWithNameItShouldAskItemToCompare() {
        ItemList list = new ItemList();
        Item book = mock(Item.class);
        list.add(book);
        list.itemWithSameName("Hello");
        verify(book).hasSameName("Hello");
    }
}
