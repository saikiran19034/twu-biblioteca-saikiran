package com.twu.biblioteca;

import com.twu.biblioteca.exceptions.InvalidDataException;
import com.twu.biblioteca.io.InputTerminal;
import com.twu.biblioteca.io.Loader;
import com.twu.biblioteca.io.Reader;
import com.twu.biblioteca.items.Book;
import com.twu.biblioteca.items.Item;
import com.twu.biblioteca.items.ItemList;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LoaderTest {
    @Test
    public void shouldReturnItemListThatAreReadFromReader() throws
            InvalidDataException {
        InputTerminal in = mock(InputTerminal.class);
        Reader reader = mock(Reader.class);
        Loader loader = new Loader(in, reader);
        when(in.hasNextLine()).thenReturn(true).thenReturn
                (false);
        when(in.nextLine()).thenReturn("book");
        Item item=mock(Book.class);
        when(reader.readFrom("book")).thenReturn(item);
        ItemList list=loader.load();
        assertTrue(list.contains(item));
    }
}
