package com.twu.biblioteca;

import com.twu.biblioteca.commands.Command;
import com.twu.biblioteca.commands.Exit;
import com.twu.biblioteca.io.InputTerminal;
import com.twu.biblioteca.io.OutputTerminal;
import com.twu.biblioteca.menu.Menu;
import com.twu.biblioteca.menu.Option;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class MenuTest {
    @Test
    public void shouldExecuteMappedCommandIfUserChoseValidOption() {
        InputTerminal in = mock(InputTerminal.class);
        OutputTerminal out = mock(OutputTerminal.class);
        Menu menu = new Menu(in, out);

        Option opt1 = mock(Option.class);
        Option opt2 = mock(Option.class);

        Command cmd1 = mock(Command.class);
        Command cmd2 = new Exit(menu);
        menu.addItem(opt1, cmd1);
        menu.addItem(opt2, cmd2);

        when(opt1.isMatcherSame("1")).thenReturn(true);
        when(opt2.isMatcherSame("0")).thenReturn(true);
        when(in.nextLine()).thenReturn("1").thenReturn("0");
        menu.drive();
        verify(cmd1).execute();
    }

    @Test
    public void shouldPromptUserThatHeChoseInvalidOptionIfOptionIsInvalid() {
        InputTerminal in = mock(InputTerminal.class);
        OutputTerminal out = mock(OutputTerminal.class);
        Menu menu = new Menu(in, out);

        Option opt1 = mock(Option.class);
        Option opt2 = mock(Option.class);

        Command cmd1 = mock(Command.class);
        Command cmd2 = new Exit(menu);

        menu.addItem(opt1, cmd1);
        menu.addItem(opt2, cmd2);

        when(in.nextLine()).thenReturn("2").thenReturn("0");
        when(opt2.isMatcherSame("0")).thenReturn(true);
        menu.drive();

        verify(out).write("Invalid Option!");
    }
}
