package com.twu.biblioteca;

import com.twu.biblioteca.exceptions.InvalidDataException;
import com.twu.biblioteca.io.MovieReader;
import com.twu.biblioteca.items.Item;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class MovieReaderTest {
    @Test
    public void shouldNotThrowExceptionIfFormatIsCorrect() throws InvalidDataException {
        MovieReader reader = new MovieReader();
        reader.readFrom("dir 1|abcd|1994|unrated\n");
    }

    @Test(expected = InvalidDataException.class)
    public void shouldThrowExceptionIfFormatIsInvalid() throws InvalidDataException {
        MovieReader reader = new MovieReader();
        reader.readFrom("dir 1|abcd|1994unrated\n");
    }

    @Test
    public void shouldReturnMovieObjectAfterParsing() throws
            InvalidDataException {
        MovieReader reader = new MovieReader();
        Item movie = reader.readFrom("dir 1|abcd|1994|unrated");
        assertTrue(movie.hasSameName("abcd"));
    }
}
