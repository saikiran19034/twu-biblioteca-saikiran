package com.twu.biblioteca;

import com.twu.biblioteca.items.Item;
import com.twu.biblioteca.items.ItemList;
import com.twu.biblioteca.items.Movie;
import com.twu.biblioteca.sections.MovieSection;
import com.twu.biblioteca.user.User;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MovieSectionTest {
    @Test
    public void shouldCheckOutMovieIfNameIsValid() {
        ItemList list = mock(ItemList.class);
        User user = mock(User.class);
        Item harry = mock(Item.class);
        when(list.itemWithSameName("harry")).thenReturn(harry);
        MovieSection section = new MovieSection(list);
        assertTrue(section.checkOut("harry", user));
    }

    @Test
    public void shouldNotCheckOutMovieIfNameIsInValid() {
        ItemList list = mock(ItemList.class);
        User user = mock(User.class);
        Item harry = mock(Item.class);
        when(list.itemWithSameName("harry")).thenReturn(harry);
        MovieSection section = new MovieSection(list);
        assertFalse(section.checkOut("hey", user));
    }

    @Test
    public void userShouldBeAbleToCheckInTheMovieHeCheckedOut() {
        ItemList list = mock(ItemList.class);
        User user = mock(User.class);
        Item harry = mock(Item.class);
        when(harry.hasSameName("harry")).thenReturn(true);
        when(list.itemWithSameName("harry")).thenReturn(harry);
        when(user.isSameUser(user)).thenReturn(true);
        MovieSection section = new MovieSection(list);
        section.checkOut("harry", user);
        assertTrue(section.checkIn("harry", user));
    }


    @Test
    public void shouldReturnStringContainingMovieAndBorrower() {
        ItemList list = mock(ItemList.class);
        User user = mock(User.class);
        when(user.toString()).thenReturn("Hero");
        Item harry = new Movie("some one", "harry", 2003, "9");
        when(list.itemWithSameName("harry")).thenReturn(harry);
        MovieSection section = new MovieSection(list);
        section.checkOut("harry", user);
        assertEquals(section.whoCheckedOutList(), harry.toString() + "==>" + "Hero\n");
    }
}
