package com.twu.biblioteca;

import com.twu.biblioteca.exceptions.InvalidDataException;
import com.twu.biblioteca.io.InputTerminal;
import com.twu.biblioteca.io.StandardInput;
import com.twu.biblioteca.io.UserDataLoader;
import com.twu.biblioteca.user.User;
import com.twu.biblioteca.user.UserList;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserDataLoaderTest {
    @Test
    public void shouldReturnListOfItemsOfNumberEqualToUserRecords() throws InvalidDataException {
        InputTerminal in = mock(StandardInput.class);
        when(in.hasNextLine()).thenReturn(true, true, false);
        when(in.nextLine()).thenReturn("000-1111|pwd|user 1|email@mail.com|1234567890").thenReturn("111-2222|pwd|user 2|email2@email.com|2134567890");
        UserDataLoader loader = new UserDataLoader(in);
        UserList list = loader.load();
        assertEquals(2, list.size());
    }

    @Test(expected = InvalidDataException.class)
    public void shouldThrowExceptionIfDataIsInvalidFormat() throws InvalidDataException {
        InputTerminal in = mock(StandardInput.class);
        when(in.hasNextLine()).thenReturn(true, true, false);
        when(in.nextLine()).thenReturn("000-1111|pwduser 1|email@mail.com|1234567890").thenReturn("111-2222|pwd|user 2|email2@email.com|2134567890");
        UserDataLoader loader = new UserDataLoader(in);
        UserList list = loader.load();
        assertEquals(2, list.size());
    }

    @Test
    public void shouldReturnTrueIfOutputFromDataLoaderContainTheSpecifiedUser() throws InvalidDataException {
        InputTerminal in = mock(StandardInput.class);
        when(in.hasNextLine()).thenReturn(true, true, false);
        when(in.nextLine()).thenReturn("000-1111|pwd|user 1|email@mail.com|1234567890").thenReturn("111-2222|pwd|user 2|email2@email.com|2134567890");
        UserDataLoader loader = new UserDataLoader(in);
        UserList list = loader.load();
        User user = new User("000-1111", "pwd", "user 1", "email@mail.com", "1234567890");
        assertTrue(list.contains(user));
    }
}
