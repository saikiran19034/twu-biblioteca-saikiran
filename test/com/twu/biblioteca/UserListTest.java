package com.twu.biblioteca;

import com.twu.biblioteca.user.User;
import com.twu.biblioteca.user.UserList;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserListTest {
    @Test
    public void tst() {
        User use1 = mock(User.class);
        User use2 = mock(User.class);
        User use3 = mock(User.class);
        UserList list = new UserList();
        list.add(use1);
        list.add(use2);
        list.add(use3);
        list.validate("user","pwd");
        verify(use1).isSameUser("user","pwd");
    }
    @Test
    public void tst2() {
        User use1 = mock(User.class);
        User use2 = mock(User.class);
        User use3 = mock(User.class);
        when(use2.isSameUser("user","pwd")).thenReturn(true);
        UserList list = new UserList();
        list.add(use1);
        list.add(use2);
        list.add(use3);
        list.validate("user","pwd");
        assertTrue(list.validate("user","pwd"));
    }

    @Test
    public void tst3() {
        User use1 = mock(User.class);
        User use2 = mock(User.class);
        User use3 = mock(User.class);
        when(use2.isSameUser("user","pwd")).thenReturn(true);
        UserList list = new UserList();
        list.add(use1);
        list.add(use2);
        list.add(use3);
        assertTrue(list.contains(use1));
    }
}
