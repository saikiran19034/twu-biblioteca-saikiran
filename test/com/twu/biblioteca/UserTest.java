package com.twu.biblioteca;

import com.twu.biblioteca.user.User;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

public class UserTest {
    @Test
    public void userNameAndPasswordShouldMatchForAuthenticating() {
        User user = new User("000-111", "pwd", "user 1", "email@mail.com",
                "1234567890");
        assertTrue(user.isSameUser("000-111","pwd"));
    }

    @Test
    public void userIdShouldMatchForSameUsers(){
        User user = new User("000-111", "pwd", "user 1", "email@mail.com",
                "1234567890");
        assertTrue(user.equals(user));
    }
}
